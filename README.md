# Division Examples

How to do division in a nice, safe way.

Like Excel but with extra safety wheels.

## Building

```
nix develop
```

## Testing

```
nix develop
ghcid --command="ghci Main.hs" --test=":run test"
```
